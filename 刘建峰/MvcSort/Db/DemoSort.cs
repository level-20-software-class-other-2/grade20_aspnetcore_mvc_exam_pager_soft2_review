using Microsoft.EntityFrameworkCore;
using MvcSort.Models;

namespace MvcSort.Db
{
    public class DemoSort:DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            var strs = "server=.;database=Demo;uid=sa;pwd=123456;";
            builder.UseSqlServer(strs);
        }
        public DbSet<Product> product { get; set; }
        public DbSet<Category> Category { get; set; }

    }
}