using System.Collections.Generic;

namespace MvcSort.Models
{
    public class Category:BaseEntity
    {
        public string CategoryName { get; set; }

        public IList<Product> product { get; set; }
    }
}