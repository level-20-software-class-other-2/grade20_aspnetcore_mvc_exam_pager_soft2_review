namespace MvcSort.Models
{
    public class Product : BaseEntity
    {
        public string ProductName { get; set; }
        public string Supplier { get; set; }
        public int Price { get; set; }
        public int Stock { get; set; }
        public int categoryId { get; set; }

        public Category category { get; set; }
    }
}