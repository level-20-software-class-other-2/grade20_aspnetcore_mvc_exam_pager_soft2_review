using System.Linq;

namespace MvcSort.Rpostting
{
    public interface IRpostting<T>
    {
        void Insert(T entity);
        void Update(T entity);
        void Delete(int id);
        IQueryable<T> Tabel{get;}
    }
}