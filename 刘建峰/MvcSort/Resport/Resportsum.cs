namespace MvcSort.Resport
{
    public class Resportsum
    {   
        public int Id { get; set; }
        public string ProductName { get; set; }
        public string Supplier { get; set; }
        public int Price { get; set; }
        public int Stock { get; set; }
        public string Remarks { get; set; }
    }
}