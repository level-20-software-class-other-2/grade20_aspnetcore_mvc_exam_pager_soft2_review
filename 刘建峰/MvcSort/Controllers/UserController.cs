using System.Linq;
using Microsoft.AspNetCore.Mvc;
using MvcSort.Models;
using MvcSort.Resport;
using MvcSort.Rpostting;

namespace MvcSort.Controllers
{
    public class UserController : Controller
    {
        public IActionResult Index(string id)
        {
            var caregoryrep = new EFpostting<Category>();
            var caregory = caregoryrep.Tabel.FirstOrDefault();
            if (caregory == null)
            {
                caregoryrep.Insert(new Category
                {
                    CategoryName = "初始化类"
                });
            }
            if (id==null)
            {
                var productrep = new EFpostting<Product>();
                var product = productrep.Tabel.Where(x => x.IsDeleted == false);
                return View(product);
            }
            else
            {
                var productrep = new EFpostting<Product>();
                var product = productrep.Tabel.Where(x => x.IsDeleted == false && x.ProductName.Contains(id));
                return View(product);
            }
        }
        public IActionResult CreateIndex(int id)
        {
            var productrep = new EFpostting<Product>();
            if (id == 0)
            {
                return View();
            }
            else
            {
                var product = productrep.Tabel.FirstOrDefault(x => x.Id == id );

                productrep.Update(product);
                return View(product);
            }

        }
        public IActionResult Save(Resportsum res)
        {
            var productrep = new EFpostting<Product>();
            if (res.Id == 0)
            {
                productrep.Insert(new Product
                {
                    Id = res.Id,
                    ProductName = res.ProductName,
                    Supplier = res.Supplier,
                    Price = res.Price,
                    Stock = res.Stock,
                    Remarks = res.Remarks,
                    categoryId = 1
                });
            }
            else
            {
                var product = productrep.Tabel.FirstOrDefault(x => x.Id == res.Id);
                product.Id = res.Id;
                product.ProductName = res.ProductName;
                product.Supplier = res.Supplier;
                product.Price = res.Price;
                product.Stock = res.Stock;
                product.Remarks = res.Remarks;
                productrep.Update(product);


            }

            return Ok();
        }
        public IActionResult Delete(int id)
        {
            var productrep = new EFpostting<Product>();
            productrep.Delete(id);
            return RedirectToAction("Index");
        }
    }
}