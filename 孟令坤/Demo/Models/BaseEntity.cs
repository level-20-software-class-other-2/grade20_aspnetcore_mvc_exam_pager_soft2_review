using System;

namespace Demo.Models
{
    public abstract class BaseEntity
    {
        public int Id { get; set; }
        public bool IsActived { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime UpdateTime { get; set; }
        public int DisplayOrder { get; set; }
        public string Creator { get; set; }
        public string Remarks { get; set; }
    }
}