namespace  Demo.Models
{
    public class Product:BaseEntity
    {
      
        public string ProductName { get; set; }
        public string Supplier { get; set; }
        public decimal Price { get; set; }
        public string Stock { get; set; }
        public int CategoryId { get; set; }

        public Category Category { get; set; }
        
    }
}