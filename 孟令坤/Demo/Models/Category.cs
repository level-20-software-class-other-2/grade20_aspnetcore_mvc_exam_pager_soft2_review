using System.Collections.Generic;
namespace Demo.Models
{
    public class Category:BaseEntity
    {
        
        public string CategoryName { get; set; }
        public IEnumerable<Product> Product { get; set; }
    }
}