$(function(){
    //查找
    $("#btnQuery").click(function(){
        let keywork = $("[name = keywork]").val()
        let obj = {
            keywork,
        }
        $.post("/product/Query",obj,function(res){
            $(".tableData").html("")
            res.forEach((item) => {
                let html = `
                <tr class="tableData">
                            <td>${item.id}</td>
                            <td>${item.productName}</td>
                            <td>${item.supplier}</td>
                            <td>${item.price}</td>
                            <td>${item.stock}</td>
                            <td>${item.remarks}</td>
                            <td>
                                <input type="button" value="编辑" onclick="btnEdit(${item.Id})">
                                <input type="button" value="删除" onclick="btnDel(${item.Id})">
                            </td>
    
                        </tr>
                `
                $("#tb").append(html)
            });
        })
    })
    //添加 
    $("#btnAdd").click(function(){
        window.location.href = "/product/CreateOnEdit"
    })
    //保存
    $("#btnSave").click(function(){
        let id = $("[ name = id]").val()
        let productName = $("[ name = productName]").val()
        let supplier = $("[ name = supplier]").val()
        let price = $("[ name = price ]").val()
        let stock = $("[ name = stock ]").val()
        let remarks = $("[ name = remarks ]").val()
        let obj = {
            id,
            productName,
            supplier,
            price,
            stock,
            remarks,
        }
        $.post("/product/Save",obj,function(){
            window.location.href = "/product/Index"
        })
    })
})
function btnEdit(Id){
    window.location.href = `/product/CreateOnEdit/${Id}`
}
function btnDel(Id){
    let a = "确认删除吗？"
    if (confirm(a)) {
        window.location.href = `/product/delete/${Id}`
    }
}