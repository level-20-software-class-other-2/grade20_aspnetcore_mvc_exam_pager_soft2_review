namespace Demo.RequestParam
{
    public class Productparam
    {
        public int Id { get; set; }
        public string ProductName { get; set; }
        public string Supplier { get; set; }
        public decimal Price { get; set; }
        public string Stock { get; set; }
        public string Remarks { get; set; }
    }
}