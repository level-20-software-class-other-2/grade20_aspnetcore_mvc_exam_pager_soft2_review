using Microsoft.EntityFrameworkCore;
using Demo.Models;
namespace Demo.Db
{
    public class DemoFirst :DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("server=.;database=Demo;uid=sa;pwd=123456;");
        }
        public DbSet<Product> Product { get; set; }
        public DbSet<Category> Category { get; set; }
    }
}