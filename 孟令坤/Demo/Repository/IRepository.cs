using Demo.Models;
using System.Linq;
namespace Demo.Repository
{
    public interface IRepository<T> where T : BaseEntity
    {
        IQueryable<T> table { get; }
        void Insert(T entity);
        void Update(T entity);
        void Delete(int Id);
    }
}