namespace MVC.RequestParam
{
    public class MallParam
    {
        public int Id { get; set; }
        public string ProductName { get; set; }
        public string Supplier { get; set; }
        public string Price { get; set; }
        public int Stock { get; set; }
    }
}