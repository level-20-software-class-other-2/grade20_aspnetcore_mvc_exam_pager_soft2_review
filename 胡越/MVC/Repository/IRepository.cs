namespace MVC.Repository
{
    public interface IRepository<T>
    {
            void Insert(T entity);
            void Update(T entity);
            void Delete(int id);
    }
}