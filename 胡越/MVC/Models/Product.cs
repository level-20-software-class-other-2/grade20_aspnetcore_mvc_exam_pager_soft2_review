using MVC.Models;
namespace MVC.Models
{
    public class Product : BaseEntity 
    {
        public string ProductName { get; set; }
        public string Supplier { get; set; }
        public string Price { get; set; }
        public int Stock { get; set; }
    }
}