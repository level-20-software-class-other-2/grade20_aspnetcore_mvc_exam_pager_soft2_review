using System.Linq;
using Mall.Controllers;
using Mall.Db;
using Microsoft.EntityFrameworkCore;
using Mall.Models;
namespace Mall.Repository
{
    public class EfRepository<T> : IRepository<T>  where T :BaseEntity
    {
        private readonly DbDemo _db;
        private readonly DbSet<T> _table;
        public IQueryable<T> table{
            get{
                return _table;
            }
        }
        public EfRepository(){
            _db=new DbDemo();
            _table=_db.Set<T>();
        }
        public void Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public void Insert(T entity)
        {
            throw new System.NotImplementedException();
        }

        public void Update(T entity)
        {
            throw new System.NotImplementedException();
        }
    }
}