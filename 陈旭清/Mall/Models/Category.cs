using Mall.Controllers;

namespace Mall.Models
{
    public class Category:BaseEntity
    {
        public string CategoryName{get;set;}
    }
}