using Mall.Controllers;
namespace Mall.Models
{
    public class Product : BaseEntity
    {
        public string ProductName { get; set; }
        public string Supplier { get; set; }
        public decimal Price { get; set; }
        public int Stock { get; set; }
    }
}