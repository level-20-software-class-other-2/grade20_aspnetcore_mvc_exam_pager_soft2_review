using Mall.Models;
using Microsoft.EntityFrameworkCore;

namespace Mall.Db
{
    public class DbDemo:DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            var Sql="server=.;database=Mall;uid=sa;pwd=123456;";
            builder.UseSqlServer(Sql);
        }
        public DbSet<Category>Category{get;set;}
        public DbSet<Product>Product{get;set;}
    }
}