using System;

namespace A.Models
{
    public class BaseEntity
    {
        public string Id{get;set;}
        public string IsActived{get;set;}
        public string IsDeleted{get;set;}
        public string CreateTime{get;set;}
        public string UpdatedTime{get;set;}
        public string DisplayOrder{get;set;}
        public string Creator{get;set;}
        public string Remarks{get;set;}

    }
}