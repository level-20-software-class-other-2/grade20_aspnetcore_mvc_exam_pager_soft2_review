using Microsoft.EntityFrameworkCore;
using MVC.Models;

namespace MVC.Db
{
    public class DemoDb:DbContext
    {
        private readonly string _Constring="server=. ;database=DmeoDb; uid=sa ;pwd=123456";


        protected override void OnConfiguring (DbContextOptionsBuilder builder)
        {
            builder.UseSqlServer(_Constring);
        }


        public DbSet<Product> Products {get; set;}
        public DbSet<Category> categories {get; set;}
    }
}