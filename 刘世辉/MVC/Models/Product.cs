namespace MVC.Models
{
    public class Product:BaseEntity
    {
        public string ProductName{get;set;}
        public string Supplier{get; set;}
        public decimal Balance{get; set;}
        public int Stock{get; set;}

        public int CategoryId{get; set;}
        public Category category{get; set;}
    }
}