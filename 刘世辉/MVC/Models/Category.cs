using System.Collections.Generic;

namespace MVC.Models
{
    public class Category:BaseEntity
    {
        public string CategoryName{get; set;}
        public IEnumerable<Product> Products{get ;set;}
    }
}