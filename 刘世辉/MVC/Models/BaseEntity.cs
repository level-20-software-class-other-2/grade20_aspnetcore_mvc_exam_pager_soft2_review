
using System;
namespace MVC.Models
{
    public abstract class BaseEntity
    {
        public int Id{get ;set ;}
        public bool IsActived{get ;set ;}
        public bool IsDelted{get ;set ;}
        public DateTime CreateTime{get ;set ;}
        public DateTime UpdateTime{get ;set ;}
        public int DisplayOrder{get ;set ;}
        public String Creator{get ;set ;}
        public string Remarks{get ;set ;}
    }
}