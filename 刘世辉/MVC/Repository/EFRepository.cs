using System.Linq;
using Microsoft.EntityFrameworkCore;
using MVC.Models;
using MVC.Repository;
namespace MVC.Repository
{
    public class EFRpository<T> : IRpository<T> where T : BaseEntity
    {
        private const int V = 0;
        private readonly MVC.Db.DemoDb _db;
        private readonly DbSet<T> _table;

        public EFRpository()
        {
            _db=new Db.DemoDb();
            _table=_db.Set<T>();
        }
        public T GetById(int id)
        {
            var entity=_table.Where(x=>x.Id==id).FirstOrDefault();
            return entity;
        }

        public IQueryable<T> Table { get
        {
            return _table.AsQueryable();
        } }

        public void Insert(T entity)
        {
            entity.IsActived=true;
            entity.IsDelted=false;
            entity.Remarks=null;
            entity.UpdateTime=System.DateTime.Now;
            entity.CreateTime=System.DateTime.Now;
            //entity.Creator=0;
            entity.DisplayOrder=0;

           // object p = _table.Insert();
            _db.SaveChanges();
            
            
        }
        public void Update(T entity)
        {
            entity.UpdateTime=System.DateTime.Now;
            _table.UpdateRange();
            _db.SaveChanges();
        }

        public void Delete(int id)
        {
          var i=  _table.Where(x=>x.IsDelted==true).FirstOrDefault();
            _db.SaveChanges();
            _table.Remove(i);
            
        }

    }
}