using System.Linq;

namespace MVC.Repository
{
    public interface  IRpository<T>
    {
        T GetById(int id);

        IQueryable<T> Table{get;}

        void Insert(T entity);
        void Update(T entity);

        void Delete(int id);
        
    }
}