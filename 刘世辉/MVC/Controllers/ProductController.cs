using System.Linq;
using Microsoft.AspNetCore.Mvc;
using MVC.Models;
using MVC.Repository;
namespace MVC.Controllers
{
    public class ProductController:Controller
    {
        
        public IActionResult Index()
        {
            


            var ProductRep=new EFRpository<Product>();
            var students= ProductRep.Table.Where(x=>x.IsDelted==false).ToList();

            return View(students);
        }

        public IActionResult Save()
        {
            return View();
        }

         public IActionResult CreateOrEdit()
        {
            return View();
        }
         public IActionResult Delete(int id)
        {
             var productRep=new EFRpository<Product>();
             var products=productRep.Table.Where(x=>x.IsDelted==false).FirstOrDefault();
             return View(products);

        }
    }
}