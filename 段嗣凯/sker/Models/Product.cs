using sker.Models;
namespace sker.Models
{
    public class Product : BaseEntity //品类
    {
        public string ProductName { get; set; }
        public string Supplier { get; set; }
        public string Price { get; set; }
        public int Stock { get; set; }
    }
}