namespace sker.Repository
{
    public interface IRepository<T>//接口
    {
            void Insert(T entity);//插入
            void Update(T entity);//更新
            void Delete(int id);//删除
    }
}