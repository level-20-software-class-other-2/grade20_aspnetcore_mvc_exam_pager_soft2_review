using sker.Repository;
using sker.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System;
using sker.Db;
namespace sker.Repository
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly DbDemo _db;
        private readonly DbSet<T> _table;
        public IQueryable<T> table{
            get{
                return _table;
            }
        }
        public EfRepository()
        {
            _db=new DbDemo();
            _table = _db.Set<T>();
        }

        public void Delete(int id)
        {
            var entity = _table.FirstOrDefault(x=>x.Id==id); 
            entity.IsDeleted=true;
            entity.IsActived=false;
            entity.UpdatedTime=DateTime.Now;
            entity.DisplayOrder=0;
            entity.Creator=0;
            entity.Remarks=null;
            _table.Update(entity);
            _db.SaveChanges();
        }

        public void Insert(T entity)
        {
            entity.IsActived=true;
            entity.IsDeleted=false;
            entity.UpdatedTime=DateTime.Now;
            entity.CreatedTime=DateTime.Now;
            entity.DisplayOrder=0;
            entity.Creator=1;
            _table.Add(entity);
            _db.SaveChanges();
        }

        public void Update(T entity)
        {
            entity.UpdatedTime=DateTime.Now;
            _table.Update(entity);
            _db.SaveChanges();
        }
    }
}