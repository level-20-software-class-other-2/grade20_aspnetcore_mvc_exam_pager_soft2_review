$(function () {
    $('#btuQuery').click(function () {
        var name = $('[name=text]').val();
        window.location.href=`/Mall/Index?name=${name}`
    })
    $('#btuAdd').click(function () {
        window.location.href='/Mall/CreateOrEdit'
    })
    $('#btuSave').click(function () {
        let Id = $('[name=Id]').val();
        let ProductName = $('[name=ProductName]').val();
        let Supplier = $('[name=Supplier]').val();
        let Price = $('[name=Price]').val();
        let Stock = $('[name=Stock]').val();

        let obj = {
            Id,
            ProductName,
            Supplier,
            Price,
            Stock 
        }
    $.post('/Mall/Save',obj,function () {
            window.location.href='/Mall/Index'
        })
    })    
    $('#btuCancel').click(function () {
            window.location.href='/Mall/Index'
        })

})
function btuDel(Id) {
    window.location.href=`/Mall/Delete/${Id}`
}
function btuEdit(Id) {
    window.location.href=`/Mall/CreateOrEdit/${Id}`
}