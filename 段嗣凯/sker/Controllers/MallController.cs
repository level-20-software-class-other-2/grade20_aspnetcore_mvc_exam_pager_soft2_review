using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using sker.Db;
using sker.Models;
using sker.Repository;
using sker.RequestParam;

namespace sker.Controllers
{
    public class MallController : Controller
    {
        public IActionResult Index(string name)
        {   
            var db= new DbDemo();
            var _db=new EfRepository<Product>();
            var date =_db.table.FirstOrDefault();
            if (date==null)
            {
                var pro = new Product{
                ProductName = "Iphone 13 pro 256G",
                Supplier = "Apple.inc",
                Price ="8850.00",
                Stock =1800,
                Remarks="我是一个莫的感情备注"
                            };
                _db.Update(pro);
            }
            var prodata = _db.table.Where(x=>x.IsDeleted==false).ToList();
            if (name!=null)
                {
                    prodata=_db.table.Where(x=>x.ProductName.Contains(name)&&x.IsDeleted==false).ToList();
                }
            return View(prodata);
        }
        public IActionResult Save(MallParam request){
            var _db = new EfRepository<Product>();
            if (request.Id==0)
            {
                _db.Insert(new Product{
                ProductName=request.ProductName,
                Supplier = request.Supplier,
                Price = request.Price,
                Stock = request.Stock});
            }else
            {
                var product = _db.table.Where(x=>x.Id==request.Id).FirstOrDefault();
                product.Id=request.Id;
                product.ProductName=request.ProductName;
                product.Supplier=request.Supplier;
                product.Price=request.Price;
                product.Stock=request.Stock;
                _db.Update(product);
            }
            return Ok();
        }
          public IActionResult Delete(int Id){
            var _db = new EfRepository<Product>();
            if (Id==0)
            {
                
            }else
            {
                _db.Delete(Id);
            }
            return RedirectToAction("index");
        }
    }
}
