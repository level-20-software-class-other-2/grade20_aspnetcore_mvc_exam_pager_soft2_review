using System;

namespace MvcDemo.Model
{
    public class BaseEntity
    {
        public string ProductId {get; set;}
        public bool IsActived {get; set;}
        public bool IsDeleted {get; set;} 
        public CreatedTime DateTime {get; set;}
        public UpdatedTime DateTime {get; set;}
        public string DisplayOrder {get; set;}
        public string Creator {get; set;}
        public string Remarks {get; set;}

        public CategoryName CategoryName {get; set;}
    }
}