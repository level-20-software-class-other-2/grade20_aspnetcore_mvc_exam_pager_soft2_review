namespace Mvc.Models{
    public class Product:BaseEntity{
        public string ProductName{get;set}
        public string Supplier{get;set}
        public string Price{get;set}
        public string Stock{get;set}
        public string CategoryId{get;set}
        public string category{get;set}
    }
}