namespace MvcSoft.Repository
{
    public interface IRepository<T>
    {

        void Insert(T entity);
        void Update(T entity);
        void Delete(int id);
    }
}