$(function(){
    $('#btnQuery').click(function(){
        let text=$('#_text').val()
        let obj={
            text
        }
        $.post('/Product/Query/',obj,function(res){
            let tb=$('#tb')
            let tr=$('.trs')
            tr.html('')
            console.log(res);
            res.forEach(item => {
                let html=`
                <tr class="trs">
                    <td>${item.id}</td>
                    <td>${item.productName}</td>
                    <td>${item.supplier}</td>
                    <td>${item.price}</td>
                    <td>${item.stock}</td>
                    <td>
                        <input type="button" value="编辑" onclick="btnEdit(${item.id})">
                        <input type="button" value="删除" onclick="btnDel(${item.id})">
                    </td>
                </tr>
                `
                tb.append(html);
            });
        })
    })
    $('#btnAdd').click(function(){
        window.location.href='/Product/CreateOrEdit/'
    })
    $('#btnSave').click(function(){
        let
            id=$('#id').val(),
            productName=$('#productName').val(),
            supplier=$('#supplier').val(),
            price=$('#price').val(),
            stock=$('#stock').val()
        let obj={
            id,
            productName,
            supplier,
            price,
            stock
        }
        $.post('/Product/Save/',obj,function(){
            window.location.href='/Product/index/'
        })
    })
    $('#btnCancel').click(function(){
        window.location.href='/Product/index/'
    })
})
function btnEdit(id){
    window.location.href=`/Product/CreateOrEdit/${id}`
}
function btnDel(id){
    console.log(id);
    if(confirm("确认删除？")){
        $.post(`/Product/Delete/${id}`,{},function(){
            window.location.href='/Product/index/'
        })
    }
}