using Microsoft.EntityFrameworkCore;
using MvcSoft.Models;

namespace MvcSoft.Db
{
    public class ProductDb:DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder builder){
            var con="server=.;database=Products;uid=sa;pwd=123456";
            builder.UseSqlServer(con);
        }

        public DbSet<Product> ProductInfo {get;set;}
        public DbSet<Category> CategoryInfo {get;set;}
    }
}