namespace MvcSoft.Requst
{
    public class ProductRequst
    {
        public int Id { get; set; }
        public string ProductName { get; set; }
        public string Supplier { get; set; }
        public decimal Price { get; set; }
        public int Stock { get; set; }
    }
    
}
