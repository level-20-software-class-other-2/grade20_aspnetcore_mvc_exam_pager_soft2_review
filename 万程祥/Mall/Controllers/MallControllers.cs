using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Mall.Models;
using Mall.Repository;

namespace Mall.Controllers
{
    public class MallController : Controller
    {
        public IActionResult Index()
        {
            var _ca=new EfRepository<Category>();
            var ca =_ca.table.FirstOrDefault(x=>x.IsDeleted==false);
            var _db=new EfRepository<Product>();
            if(ca==null){
                _ca.Insert(new Category{
                     CategoryName="数码"
                    
                 });
                 _db.Insert(new Product{
                     ProductName="iPhone 13 Pro 256G",
                     Supplier="Apple .lnc",
                     Price=8850,
                     Stock=1800,
                     Remarks="我是一个莫得感情的备注",
                     CategoryId=1
                     
                 });             
             _ca.Insert(new Category{
                     CategoryName="数码"
                    
                 });
                 _db.Insert(new Product{
                     ProductName="iPhone 13 Pro 256G",
                     Supplier="Apple .lnc",
                     Price=8850,
                     Stock=1800,
                     Remarks="我是一个莫得感情的备注",
                     CategoryId=2
                     
                 });
                  _ca.Insert(new Category{
                     CategoryName="数码"
                    
                 });
                 _db.Insert(new Product{
                     ProductName="iPhone 13 Pro 256G",
                     Supplier="Apple .lnc",
                     Price=8850,
                     Stock=1800,
                     Remarks="我是一个莫得感情的备注",
                     CategoryId=3 
                 });
            }
            var　date=_db.table.Where(x=>x.IsDeleted==false).ToList();
            return View(date);
        }
      public IActionResult Delete(int Id)
      {
          var _db=new EfRepository<Product>();
          var entity=_db.table.Where(x=>x.Id==Id).ToList();
          _db.Deleted(Id);
          return RedirectToAction("index");
      }
    }
}
