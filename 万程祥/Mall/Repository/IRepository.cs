namespace Mall.Repository
{
    public interface IRepository<T>
    {
        void Insert(T entity);
        void Update (T entity);
        void Deleted (int Id);
    }
}