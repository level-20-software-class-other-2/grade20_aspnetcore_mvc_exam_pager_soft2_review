using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using Mall.Models;
using Mall.Db;

namespace Mall.Repository
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly DbMall _db;
        private readonly DbSet<T> _table;

        public IQueryable<T> table
        {
            get
            {
                return _table;
            }
        }
        public EfRepository()
        {
            _db=new DbMall();
            _table=_db.Set<T>();
        }
        public void Deleted(int Id)
        {
            var entity=_table.FirstOrDefault(x=>x.Id==Id);
            entity.UpdatedTime=DateTime.Now;
            entity.IsDeleted=true;
            _table.Update(entity);
            _db.SaveChanges();
        }

        public void Insert(T entity)
        {
            entity.IsActived=true;
            entity.IsDeleted=false;
            entity.CreatedTime=DateTime.Now;
            entity.UpdatedTime=DateTime.Now;
            entity.Creator=0;
            entity.DisplayOrder=0;
            entity.Remarks=null;
            _table.Add(entity);
            _db.SaveChanges();
        }

        public void Update(T entity)
        {
            entity.UpdatedTime=DateTime.Now;
            _table.Update(entity);
            _db.SaveChanges();
        }
    }
}