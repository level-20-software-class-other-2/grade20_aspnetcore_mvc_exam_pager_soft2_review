using Microsoft.EntityFrameworkCore;
using Mall.Models;

namespace Mall.Db
{
    public class DbMall:DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
           var connectstring = "server=.;database=Mall2;uid=sa;pwd=123456;";
           builder.UseSqlServer(connectstring);
        }
        public DbSet<Product> product {set;get;}
        public DbSet<Category> catrgory {set;get;}
    }
}