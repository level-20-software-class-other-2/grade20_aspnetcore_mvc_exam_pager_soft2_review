using System.Collections.Generic;

namespace Mall.Models
{
    public class Category:BaseEntity
    {
        public string CategoryName {set;get;}
        public IEnumerable<Product> product{set;get;}
    }
}