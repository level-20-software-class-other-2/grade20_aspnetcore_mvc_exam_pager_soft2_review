namespace  Mall.Models
{
    public class Product:BaseEntity
    {
        public string ProductName {set;get;}
        public string Supplier {set;get;}
        public int Price {set;get;}
        public int Stock {set;get;}
        public int CategoryId{set;get;}
        public Category catrgory {set;get;}
    }
}