using System;
using System.Linq;

namespace Mall.Models
{
    public class BaseEntity
    {
        public int Id {set;get;}
        public bool IsActived {set;get;}
        public bool IsDeleted {set;get;}
        public DateTime CreatedTime {set;get;}
        public DateTime UpdatedTime {set;get;}

        public int DisplayOrder {set;get;}
        public int Creator {set;get;}
        public string  Remarks {set;get;}
    }
}