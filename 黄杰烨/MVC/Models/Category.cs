using MVC.Models;
namespace MVC.Models
{
    public class Category : BaseEntity 
    {
        public string CategoryName { get; set; }
    }
}