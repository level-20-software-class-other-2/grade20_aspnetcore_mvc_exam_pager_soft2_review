

using Microsoft.EntityFrameworkCore;
using mvc.Models;

namespace mvc.Db
{
    public class ProductDb:DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            var str="server=.;database=ProductDb;uid=sa;pwd=123456;";
            builder.UseSqlServer(str);
        }

        public DbSet<Product> ProductInfo{get;set;}
        public DbSet<Category> Category{get;set;}
    }
}

