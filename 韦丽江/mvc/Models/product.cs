namespace mvc.Models
{
   using mvc.Models;
   
   
    public class Product:BaseEntity
    {
        public string ProductName{get;set;}

        public string Supplier{get;set;}
        
        public decimal Price{get;set;}

        public int Stock{get;set;}
        public int CategoryId{get;set;}
        public Category Categor{get;set;}
    }
}