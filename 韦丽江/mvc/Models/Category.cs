using System.Collections.Generic;
namespace mvc.Models
{
    public class Category:BaseEntity{
        
        public string CategoryName{get;set;}

        public IEnumerable<Product> Product{get;set;}
    }
}