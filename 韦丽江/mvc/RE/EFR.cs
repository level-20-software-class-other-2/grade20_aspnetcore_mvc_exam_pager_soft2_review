using System.Linq;
using mvc.Models;
using System;
using mvc.Db;
using Microsoft.EntityFrameworkCore;
using mvc.RE;


namespace mvc.RE
{
    public class EFR<T>:IER<T>where T:BaseEntity
    {
        private readonly ProductDb _db;
        private readonly DbSet<T> _table;

        public EFR(){
            _db =new ProductDb();
            _table=_db.Set<T>();

        }

        public IQueryable<T> Table{
            get{
                return _table.AsQueryable();
            }
        }

        public void Delete(int id){
            var table=_table.Where(x=>x.Id==id).FirstOrDefault();
            table.IsDeleted=true;
            table.UpdatedTime=DateTime.Now;
            _table.Update(table);
            _db.SaveChanges();
        }


        public void Insert(T entity){
            entity.IsActived=true;
            entity.IsDeleted=false;
            entity.CreatedTime=DateTime.Now;
            entity.UpdatedTime=DateTime.Now;
            entity.DisplayOrder=0;
            _table.Add(entity);
            _db.SaveChanges();


        }

        public void Update( T entity){
            entity.UpdatedTime=DateTime.Now;
            _table.Update(entity);
            _db.SaveChanges();
        }
    }
}