namespace mvc.RE
{
    public interface IER<T>
    {
        void Insert(T entity);
        void Update(T entity);

        void Delete(int id);
    }
}